import colors from 'vuetify/es5/util/colors';

export default {
	// Disable server-side rendering (https://go.nuxtjs.dev/ssr-mode)
	ssr: false,

	// Global page headers (https://go.nuxtjs.dev/config-head)
	head: {
		titleTemplate: '%s - previsao',
		title: 'previsao',
		meta: [
			{
				charset: 'utf-8',
			},
			{
				name: 'viewport',
				content: 'width=device-width, initial-scale=1',
			},
			{
				hid: 'description',
				name: 'description',
				content: process.env.npm_package_description || '',
			},
		],
		link: [
			{
				rel: 'icon',
				type: 'image/x-icon',
				href: '/favicon.ico',
			},
		],
	},

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css: ['~/assets/styles/index.css'],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins: [],

	// Auto import components (https://go.nuxtjs.dev/config-components)
	components: true,

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules: [
		// https://go.nuxtjs.dev/eslint
		'@nuxtjs/eslint-module',
		// https://go.nuxtjs.dev/vuetify
		'@nuxtjs/vuetify',
	],

	// Modules (https://go.nuxtjs.dev/config-modules)
	modules: [
		// https://go.nuxtjs.dev/axios
		'@nuxtjs/axios',
	],

	// Axios module configuration (https://go.nuxtjs.dev/config-axios)
	axios: {},

	// Vuetify module configuration (https://go.nuxtjs.dev/config-vuetify)
	vuetify: {
		customVariables: ['~/assets/variables.scss'],
		optionsPath: '~/plugins/vuetify.options.js',
	},

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		transpile: ['vuex-module-decorators'],
		/*
		 ** You can extend webpack config here
		 */
		extend(config, ctx) {},
	},
};
