import colors from 'vuetify/es5/util/colors';
import pt from 'vuetify/es5/locale/pt';

export default {
	theme: {
		dark: false,
		themes: {
			light: {
				primary: '#4d83cf',
				accent: '#84c554',
				secondary: '#314921',
				info: '#5bc0de',
				warning: '#f0ad4e',
				error: '#d9534f',
				success: '#2E7D32',
			},
		},
	},
	lang: {
		locales: {
			pt,
		},
		current: 'pt',
	},
	icons: {
		iconfont: 'mdiSvg', // 'mdi' || 'mdiSvg' || 'md' || 'fa' || 'fa4' || 'faSvg',
	},
};
