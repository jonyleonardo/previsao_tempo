# README

# Previsso do Tempo

## Build Setup

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).

# PARA REALIZACAO DESTE PROJETO FORAM USADOS:

$ Nuxt.js
$ Vuetify.js
$ Pug.js
$ Lodash
\$ Axios

\$ No VSCode, deixo a Extensao Auto Format Vue / Vue Sniptts 2/3 'desligada', pois ele causa conflito com a configuração do Eslint e com a configuração do prettier;
